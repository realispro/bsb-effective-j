package pl.bsb.cc;

// import

import java.util.Objects;

public class User {

    // public (protected, default, private) static final

    private long id;

    private String userName;

    private String passwordHash;

    private boolean admin;

    public User(long id, String userName, String passwordHash, boolean admin) {
        this.id = id;
        this.userName = userName;
        this.passwordHash = passwordHash;
        this.admin = admin;
    }

    public User() {
    }

    // business methods

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPasswordHash() {
        return passwordHash;
    }

    public void setPasswordHash(String passwordHash) {
        this.passwordHash = passwordHash;
    }

    public boolean isAdmin() {
        return admin;
    }

    public void setAdmin(boolean admin) {
        this.admin = admin;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return id == user.id &&
                admin == user.admin &&
                Objects.equals(userName, user.userName) &&
                Objects.equals(passwordHash, user.passwordHash);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, userName, passwordHash, admin);
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", userName='" + userName + '\'' +
                ", passwordHash='" + passwordHash + '\'' +
                ", admin=" + admin +
                '}';
    }
}
