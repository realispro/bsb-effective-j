package pl.bsb.cc;

/**
 * The class responsible for user management
 */
public class UserManager {

    public static final String OBJECT_DESCRIPTION = "User Management Class";

    private Object userRepository;

    public boolean validateUser(User user){
        return user.isAdmin();
    }

}
