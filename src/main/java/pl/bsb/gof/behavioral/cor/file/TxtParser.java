package pl.bsb.gof.behavioral.cor.file;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class TxtParser extends Parser {

    @Override
    protected void parseFile(File file) throws IOException {
        Files
                .lines(Paths.get(file.getAbsolutePath()))
                .forEach(System.out::println);
    }

    @Override
    protected String getSupportedFileExtension() {
        return ".txt";
    }
}
