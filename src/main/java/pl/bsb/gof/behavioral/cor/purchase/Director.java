package pl.bsb.gof.behavioral.cor.purchase;

public class Director extends Approver {

    public Director(String name) {
        super(name, 1000);
    }
}
