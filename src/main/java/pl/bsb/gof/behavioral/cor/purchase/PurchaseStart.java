package pl.bsb.gof.behavioral.cor.purchase;

public class PurchaseStart {

    public static void main(String[] args) {
        System.out.println("let's process a purchase");

        Purchase p = new Purchase(243162361, "nowe biuro");
        Purchase p1 = new Purchase(50000, "samochod dla sprzedawcy");
        Purchase p2 = new Purchase(50, "ryza papieru");
        Purchase p3 = new Purchase(650, "krzeslo");

        Approver approver = getApprover();
        approver.approve(p1);
        System.out.println("purchase state: " + p1);
        approver.approve(p2);
        System.out.println("purchase state: " + p2);
        approver.approve(p3);
        System.out.println("purchase state: " + p3);

        approver.approve(p);
        System.out.println("purchase state: " + p);
    }

    private static Approver getApprover(){
        President president = new President("Jan Kowalski");
        Director director = new Director("inz. Karwowski");
        Secretary secretary = new Secretary("Katarzyna Olsztyńska");
        secretary.setNextApprover(director);
        director.setNextApprover(president);

        return secretary;
    }
}
