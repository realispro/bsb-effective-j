package pl.bsb.gof.behavioral.cor.file;

import java.io.File;
import java.io.IOException;

public abstract class Parser {

    protected Parser parser;

    public void setParser(Parser parser) {
        this.parser = parser;
    }

    public void parse(File file) throws IOException {
        if (file.getName().endsWith(getSupportedFileExtension())) {
            parseFile(file);
        } else {
            if (parser != null) {
                parser.parse(file);
            } else {
                System.out.println("no handler found for processing request " + file.getName());
            }
        }
    }

    protected abstract void parseFile(File file) throws IOException;

    protected abstract String getSupportedFileExtension();
}
