package pl.bsb.gof.behavioral.cor.purchase;

public class Secretary extends Approver {

    public Secretary(String name) {
        super(name, 100);
    }
}