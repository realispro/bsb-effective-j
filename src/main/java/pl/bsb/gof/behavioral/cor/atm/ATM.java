package pl.bsb.gof.behavioral.cor.atm;

public class ATM {

    private Dispenser dispenser;

    public ATM() {
        // TODO construct dispenser chain
        Dispenser dispenser100 = new Dispenser(100);
        Dispenser dispenser50 = new Dispenser(50);
        Dispenser dispenser20 = new Dispenser(20);
        Dispenser dispenser10 = new Dispenser(10);

        dispenser100.setNext(dispenser50);
        dispenser50.setNext(dispenser20);
        dispenser20.setNext(dispenser10);

        dispenser = dispenser100;
    }

    public void withdraw(int amount){
        Request request = new Request(amount);
        dispenser.dispense(request);
    }





}
