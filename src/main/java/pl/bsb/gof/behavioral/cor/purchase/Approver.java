package pl.bsb.gof.behavioral.cor.purchase;

public abstract class Approver {

    private Approver nextApprover;

    private String name;

    private int max;

    public Approver(String name, int max) {
        this.name = name;
        this.max = max;
    }

    public void approve(Purchase p) {
        if (max >= p.getAmount()) {
            System.out.println("Przyjęte do realizacji: " + p.getPurpose());
            p.approve(name);
        } else {
            if (nextApprover != null) {
                nextApprover.approve(p);
            } else {
                throw new IllegalStateException("Join the Board meeting");
            }
        }
    }

    public String getName() {
        return name;
    }

    public void setNextApprover(Approver nextApprover) {
        this.nextApprover = nextApprover;
    }
}
