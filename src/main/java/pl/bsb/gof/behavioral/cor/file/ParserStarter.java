package pl.bsb.gof.behavioral.cor.file;

import java.io.File;
import java.io.IOException;

public class ParserStarter {

    public static void main(String[] args) throws IOException {
        System.out.println("let's parse a file");

        File file = new File("./src/main/resources/cor/cor.xml");
        Parser parser = getParser();
        parser.parse(file);
    }

    public static Parser getParser(){
        Parser parser = new TxtParser();
        parser.setParser(new CsvParser());
        return parser;

    }



}
