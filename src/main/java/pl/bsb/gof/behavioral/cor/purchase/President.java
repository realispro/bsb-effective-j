package pl.bsb.gof.behavioral.cor.purchase;

public class President extends Approver {

    public President(String name) {
        super(name, 100000);
    }
}
