package pl.bsb.gof.behavioral.cor.atm;

public class Dispenser {

    private Dispenser next;

    private int nominal;

    public Dispenser(int nominal) {
        this.nominal = nominal;
    }

    public void setNext(Dispenser next) {
        this.next = next;
    }

    public void dispense(Request request){

        int reminder = dispenseAndGetReminder(request);

        if(reminder>0){
            if(next!=null){
                request.setAmount(reminder);
                next.dispense(request);
            } else {
                throw new IllegalStateException("non processable reminder " + reminder);
            }
        }
    }

    private int dispenseAndGetReminder(Request request){
        int count = request.getAmount()/nominal;
        if(count>0){
            System.out.println(count + " notes of " + nominal + "$");
        }
        return request.getAmount()%nominal;
    }
}
