package pl.bsb.gof.behavioral.mediator.chat;


public class ChatPart {

    private Chat chat;

    public ChatPart(Chat chat) {
        this.chat = chat;
        chat.addPart(this);
    }

    public void send(String message){
        chat.dispatch(message, this);
    }

    public void receive(String message){
        System.out.println("someone send a letter: " + message);
    }
}
