package pl.bsb.gof.behavioral.mediator.chat;

/**
 * Created by xdzm on 2016-06-17.
 */
public class LaunchChat {

    public static void main(String[] args) {

        Chat chat = new Chat();

        ChatPart cp1 = new ChatPart(chat);
        ChatPart cp2 = new ChatPart(chat);
        ChatPart cp3 = new ChatPart(chat);

        cp1.send("Hey man!");
        cp2.send("How are You?");
        cp3.send("cannot speak now.");
    }
}
