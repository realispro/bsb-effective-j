package pl.bsb.gof.behavioral.mediator.chat;

import java.util.ArrayList;
import java.util.List;

public class Chat {

    private List<ChatPart> parts = new ArrayList<>();

    public void dispatch(String message, ChatPart sender){
        parts.stream().filter(p->p!=sender).forEach(p->p.receive(message));
    }

    public void addPart(ChatPart part){
        parts.add(part);
    }


}
