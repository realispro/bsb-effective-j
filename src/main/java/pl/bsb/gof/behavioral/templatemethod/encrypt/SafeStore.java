package pl.bsb.gof.behavioral.templatemethod.encrypt;


import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public abstract class SafeStore {


    public boolean store(Object o){
        try {
            byte[] toStore = encrypt(o.toString());
            storeEncrypted(toStore);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    protected abstract byte[] encrypt(String s) throws Exception;


    private final void storeEncrypted(byte[] bytes){

        try(BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream("safe.store"))){
            bos.write(bytes);
        }  catch (IOException e) {
            e.printStackTrace();
        }
    }
}
