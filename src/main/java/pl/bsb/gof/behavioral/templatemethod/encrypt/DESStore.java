package pl.bsb.gof.behavioral.templatemethod.encrypt;

public class DESStore extends CipherStore{
    @Override
    protected byte[] getKey() {
        return new byte[]{'1', '2', '3', '4', '5', '6', '7', '8'};
    }

    @Override
    protected String getAlgorithm() {
        return "DES";
    }
}
