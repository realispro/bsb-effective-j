package pl.bsb.gof.behavioral.observer.press;

public class RealObserver implements Observer{
    @Override
    public void notify(News news) {
        System.out.println("Read news: " + news);
    }
}
