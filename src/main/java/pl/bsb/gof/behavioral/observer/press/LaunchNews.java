package pl.bsb.gof.behavioral.observer.press;

import java.util.Arrays;

public class LaunchNews {

    public static void main(String[] args) {

        PressOffice po = new PressOffice();
        po.addObserver(new RealObserver());
        po.addObserver(new RealObserver());
        po.addObserver(new RealObserver());
        po.addObserver(p-> System.out.println("ad hoc listener: " + p));

        News n1 = new News("Mundial: Poland won!", "Polska mistrzem Polski");
        News n2 = new News("Chemitrails are fake", "Chemitrails doesn't exists");

        po.dayWork(Arrays.asList(n1,n2));

    }
}
