package pl.bsb.gof.behavioral.observer.followers;

import java.util.ArrayList;
import java.util.List;

public class Profile {

    private List<Follower> followers = new ArrayList<>();

    public void publish(String message) {
        Post p = new Post(message);
        System.out.println("post generated: " + p.getMessage());
        followers.forEach(f->f.notify(p));
    }


    public void addFollower(Follower follower){
        followers.add(follower);
    }



}
