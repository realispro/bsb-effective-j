package pl.bsb.gof.behavioral.observer.followers;

public class RealFollower implements Follower {
    @Override
    public void notify(Post post) {
        System.out.println("notification received. new post: " + post);
    }
}
