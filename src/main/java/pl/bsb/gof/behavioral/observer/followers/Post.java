package pl.bsb.gof.behavioral.observer.followers;

public class Post {

    private String message;

    public Post(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    @Override
    public String toString() {
        return "Post{" +
                "message='" + message + '\'' +
                '}';
    }
}
