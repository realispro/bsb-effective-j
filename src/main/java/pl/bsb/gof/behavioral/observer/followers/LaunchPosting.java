package pl.bsb.gof.behavioral.observer.followers;

public class LaunchPosting {

    public static void main(String[] args) {

        Profile profile = new Profile();

        profile.addFollower(new RealFollower());
        profile.addFollower(new RealFollower());
        profile.addFollower(p-> System.out.println("post received:" + p));

        profile.publish("totally unimportant message 1");
        profile.publish("totally unimportant message 2");
    }
}
