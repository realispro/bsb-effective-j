package pl.bsb.gof.behavioral.observer.press;

@FunctionalInterface
public interface Observer {
    void notify(News news);
}
