package pl.bsb.gof.behavioral.observer.followers;

@FunctionalInterface
public interface Follower {

    void notify(Post post);
}
