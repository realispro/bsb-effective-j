package pl.bsb.gof.behavioral.observer.press;

import java.util.ArrayList;
import java.util.List;

public class PressOffice {

    List<Observer> observers = new ArrayList<>();

    public void dayWork(List<News> news){
        news.forEach(this::broadcast);
    }

    private void broadcast(News news){
        System.out.println("broadcasting news: " + news);
        observers.forEach(o -> o.notify(news));
    }

    public void addObserver(Observer observer) {
        observers.add(observer);
    }


}
