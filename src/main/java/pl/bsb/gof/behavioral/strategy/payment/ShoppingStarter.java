package pl.bsb.gof.behavioral.strategy.payment;

public class ShoppingStarter {

    public static void main(String[] args) {
        System.out.println("let's buy smth!");

        Shopping s = new Shopping();

        s.addItem(new Item("mleko", 3, 2.2));
        s.addItem(new Item("Ipa", 24, 3.5));
        s.addItem(new Item("paluszki", 2, 1.79));



        Paypal p = new Paypal("marcin", "secret");
        CreditCard cc = new CreditCard("853272", 123);

        PaymentMethod pm = cc;
        s.pay(pm);

    }
}