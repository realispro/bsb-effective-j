package pl.bsb.gof.behavioral.strategy.encrypt;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.security.Key;

public abstract class CipherEncryption /*extends SafeStore */implements Encryption{

    protected abstract byte[] getKey();

    protected abstract String getAlgorithm();

    @Override
    public byte[] encrypt(String s) throws Exception {
        Key key = new SecretKeySpec(getKey(), getAlgorithm());
        Cipher c = Cipher.getInstance(getAlgorithm());
        c.init(Cipher.ENCRYPT_MODE, key);
        return c.doFinal(s.getBytes());
    }
}
