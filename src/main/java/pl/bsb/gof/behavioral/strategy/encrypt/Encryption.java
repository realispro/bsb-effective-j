package pl.bsb.gof.behavioral.strategy.encrypt;

public interface Encryption {

    byte[] encrypt(String s) throws Exception;
}
