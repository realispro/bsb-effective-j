package pl.bsb.gof.behavioral.strategy.payment;

public interface PaymentMethod {

    void charge(double amount);

}
