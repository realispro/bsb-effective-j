package pl.bsb.gof.behavioral.strategy.encrypt;


public class EncryptStarter {

    public static void main(String[] args){
        SafeStore store = new SafeStore();
        Encryption encryption = new AESEncryption();
        boolean stored = store.store("hide it!", encryption);
        System.out.println("safely stored? " + stored);
    }

}
