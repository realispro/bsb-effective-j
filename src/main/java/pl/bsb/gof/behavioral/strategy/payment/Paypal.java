package pl.bsb.gof.behavioral.strategy.payment;

public class Paypal implements PaymentMethod{

    private String accountName;

    private String password;


    public Paypal(String accountName, String password) {
        this.accountName = accountName;
        this.password = password;
    }

    @Override
    public void charge(double value) {
        System.out.println("connecting to paypal service, account name: " + accountName);
    }


}
