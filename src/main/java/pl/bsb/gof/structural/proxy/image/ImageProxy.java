package pl.bsb.gof.structural.proxy.image;

public class ImageProxy implements Image{

    private String fileName;

    public ImageProxy(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public byte[] getData() {
        return new RealImage(fileName).getData();
    }
}
