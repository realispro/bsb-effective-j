package pl.bsb.gof.structural.proxy.image;

public interface Image {

    byte[] getData();

}
