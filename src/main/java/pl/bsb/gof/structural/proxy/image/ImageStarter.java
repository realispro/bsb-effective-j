package pl.bsb.gof.structural.proxy.image;

public class ImageStarter {

    public static void main(String[] args) {
        System.out.println("let's display some images");

        Image i1 = new ImageProxy("logo.jpg");
        Image i2 = new ImageProxy("logo2.jpg");
        Image i3 = new ImageProxy("logo3.jpg");
        Image i4 = new ImageProxy("logo4.jpg");


        byte[] data = i1.getData();
        System.out.println("displaying image: " + new String(data));
    }
}
