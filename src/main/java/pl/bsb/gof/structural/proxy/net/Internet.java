package pl.bsb.gof.structural.proxy.net;

public interface Internet {


    void connectTo(String address);
}
