package pl.bsb.gof.structural.proxy.image;

public class RealImage implements Image{

    private byte[] data;

    public RealImage(String fileName){
        // loading image data
        System.out.println("loading data from file " + fileName);
        data = fileName.getBytes();

    }

    @Override
    public byte[] getData() {
        return data;
    }
}
