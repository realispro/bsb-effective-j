package pl.bsb.gof.structural.proxy.net;


import pl.bsb.gof.structural.proxy.net.internet.RealNetwork;

public class InternetStarter {

    public static void main(String[] args) {
        System.out.println("lets surf!");

        Internet internet = getInternet();

        internet.connectTo("wp.pl");
        internet.connectTo("facebook.com/events");

    }

    private static Internet getInternet() {
        return new ProxyNetwork(new RealNetwork());
    }

    public static class ProxyNetwork implements Internet{
        Internet real;

        public ProxyNetwork(Internet real) {
            this.real = real;
        }

        @Override
        public void connectTo(String address) {
            if(address.contains("facebook.com")){
                throw new RuntimeException("Forbidden");
            } else {
                real.connectTo(address);
            }
        }
    }
}
