package pl.bsb.gof.structural.decorator.sandwich;

public /*abstract*/ class SandwichDecorator implements Sandwich{

    private Sandwich sandwich;

    private String part;

    public SandwichDecorator(Sandwich sandwich, String part){
        this.sandwich = sandwich;
        this.part = part;
    }
    @Override
    public String content() {
        return sandwich.content() + ", " + part;
    }
    /*protected abstract String getPart();*/
}
