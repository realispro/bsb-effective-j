package pl.bsb.gof.structural.decorator.sandwich;

public class SandwichStarter {

    public static void main(String[] args) {
        System.out.println("let's eat some sandwich");

        Sandwich sandwich = new WhiteBreadSandwich();
        sandwich = new SandwichDecorator(sandwich, "cheese");
        sandwich = new SandwichDecorator(sandwich, "cheese");
        sandwich = new SandwichDecorator(sandwich, "cheese");
        sandwich = new SandwichDecorator(sandwich, "keczap");
        sandwich = new SandwichDecorator(sandwich, "tomato");



        /*sandwich = new CheeseDecorator(sandwich);
        sandwich = new CheeseDecorator(sandwich);
        sandwich = new KeczapDecorator(sandwich);
        sandwich = new TomatoDecorator(sandwich);
*/
        System.out.println("eating " + sandwich.content());
    }
}
