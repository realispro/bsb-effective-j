package pl.bsb.gof.structural.decorator.email;

public class FormalEmailDecorator extends EmailDecorator{

    public FormalEmailDecorator(Email email) {
        super(email);
    }

    @Override
    protected String getSignature() {
        return "Regards,\nJoe Doe\nIT Specialist";
    }
}
