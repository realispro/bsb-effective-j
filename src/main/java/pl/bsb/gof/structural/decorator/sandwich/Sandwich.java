package pl.bsb.gof.structural.decorator.sandwich;

public interface Sandwich {

    String content();
}
