package pl.bsb.gof.structural.decorator.email;

public class EmailStarter {


    public static void main(String[] args) {
        System.out.println("let's send some email");

        Email email = new EmailMessage("GoF training", "3 days training");

        email = new UnformalEmailDecorator(email);
        //email = new FormalEmailDecorator(email);

        System.out.println("sending email: title=[" + email.getTitle() + "], content=[" + email.getContent() + "]");

    }

}
