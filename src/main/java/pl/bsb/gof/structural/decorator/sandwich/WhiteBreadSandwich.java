package pl.bsb.gof.structural.decorator.sandwich;

public class WhiteBreadSandwich implements Sandwich{
    @Override
    public String content() {
        return "white bread";
    }
}
