package pl.bsb.gof.structural.decorator.email;

public class UnformalEmailDecorator extends EmailDecorator{

    public UnformalEmailDecorator(Email email) {
        super(email);
    }

    @Override
    protected String getSignature() {
        return "Cheers\nJoe";
    }
}
