package pl.bsb.gof.structural.flyweight.wallet;

import java.util.ArrayList;
import java.util.List;

public class WalletStarter {

    public static void main(String[] args) {
        System.out.println("let's put some coins into wallet");

        List<Coin> wallet = new ArrayList<>();
        wallet.add(CoinPool.getCoin(5, Currency.EURO));
        wallet.add(CoinPool.getCoin(5, Currency.EURO));
        wallet.add(CoinPool.getCoin(5, Currency.EURO));
        wallet.add(CoinPool.getCoin(2, Currency.EURO));
        wallet.add(CoinPool.getCoin(5, Currency.PLN));
        wallet.add(CoinPool.getCoin(5, Currency.PLN));
        wallet.add(CoinPool.getCoin(1, Currency.PLN));
        wallet.add(CoinPool.getCoin(1, Currency.PLN));
        wallet.add(CoinPool.getCoin(1, Currency.PLN));

        int euros = wallet.stream()
                .filter(c->c.getCurrency()==Currency.EURO)
                .map(c->c.getValue())
                .reduce((v1,v2)->v1+v2)
                .get();
        int plns = wallet.stream()
                .filter(c->c.getCurrency()==Currency.PLN)
                .map(c->c.getValue())
                .reduce((v1,v2)->v1+v2)
                .get();

        System.out.println("euros = " + euros);
        System.out.println("plns = " + plns);





        System.out.println("wallet content: " + wallet);



    }
}
