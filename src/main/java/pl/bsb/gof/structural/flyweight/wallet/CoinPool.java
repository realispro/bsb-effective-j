package pl.bsb.gof.structural.flyweight.wallet;

import java.util.HashMap;
import java.util.Map;

public class CoinPool {

    private static Map<String, Coin> coins = new HashMap<>();

    private static Map<Currency, Map<Integer, Coin>> coins2 = new HashMap<>();

    private static Map<CoinKey, Coin> coins3 = new HashMap<>();

    public static Coin getCoin(int value, Currency currency){
        Coin coin = coins.get(createKey(value, currency));
        if(coin == null){
            coin = new Coin(value, currency);
            coins.put(createKey(value, currency), coin);
        }
        return coin;
    }

    private static String createKey(int value, Currency currency){
        return value + currency.toString();
    }
}
