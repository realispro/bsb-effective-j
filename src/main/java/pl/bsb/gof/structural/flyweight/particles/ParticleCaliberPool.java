package pl.bsb.gof.structural.flyweight.particles;

import java.util.HashMap;
import java.util.Map;

public class ParticleCaliberPool {

    private static Map<Integer, ParticleCaliber> calibers = new HashMap<>();

    public static ParticleCaliber getParticleCaliber(int caliber){

        ParticleCaliber particleCaliber = calibers.get(caliber);
        if(particleCaliber==null){
            particleCaliber = new ParticleCaliber(caliber);
            calibers.put(caliber, particleCaliber);
        }
        return particleCaliber;
    }

}
