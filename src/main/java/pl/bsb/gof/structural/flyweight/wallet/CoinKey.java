package pl.bsb.gof.structural.flyweight.wallet;

import java.util.Objects;

public class CoinKey {

    private int value;

    private Currency currency;

    public CoinKey(int value, Currency currency) {
        this.value = value;
        this.currency = currency;
    }

    public int getValue() {
        return value;
    }

    public Currency getCurrency() {
        return currency;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CoinKey coinKey = (CoinKey) o;
        return value == coinKey.value &&
                currency == coinKey.currency;
    }

    @Override
    public int hashCode() {
        return Objects.hash(value, currency);
    }
}
