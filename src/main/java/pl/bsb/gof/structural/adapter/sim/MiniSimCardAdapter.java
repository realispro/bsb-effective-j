package pl.bsb.gof.structural.adapter.sim;

public class MiniSimCardAdapter implements Sim{

    private MiniSimCard miniSimCard;

    public MiniSimCardAdapter(MiniSimCard miniSimCard) {
        this.miniSimCard = miniSimCard;
    }

    @Override
    public String getId() {
        return new String(miniSimCard.getId());
    }
}
