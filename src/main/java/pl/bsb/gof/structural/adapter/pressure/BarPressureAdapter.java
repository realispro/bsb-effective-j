package pl.bsb.gof.structural.adapter.pressure;

import pl.bsb.gof.structural.adapter.pressure.bar.BarPressureProvider;

public class BarPressureAdapter implements PascalPressure {

    private BarPressureProvider barPressureProvider;

    public BarPressureAdapter(BarPressureProvider barPressureProvider) {
        this.barPressureProvider = barPressureProvider;
    }

    @Override
    public float getPressure() {
        float value = barPressureProvider.getPressure();
        System.out.println("value = " + value);
        return  value * 100000;
    }
}
