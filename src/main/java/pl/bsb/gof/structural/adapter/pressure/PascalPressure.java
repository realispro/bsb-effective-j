package pl.bsb.gof.structural.adapter.pressure;

public interface PascalPressure {

    float getPressure();
}
