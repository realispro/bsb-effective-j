package pl.bsb.gof.structural.adapter.pressure;

import pl.bsb.gof.structural.adapter.pressure.bar.BarPressureProvider;

public class PressureStarter {

    public static void main(String[] args) {
        System.out.println("let's measure a pressure !");

        BarPressureProvider barPressureProvider = new BarPressureProvider();

        PascalPressure pascalPressure = new BarPressureAdapter(barPressureProvider);
        Display display = new Display();
        display.showPresure(pascalPressure);

    }
}
