package pl.bsb.gof.structural.adapter.sim;

public class SimStarter {

    public static void main(String[] args) {
        System.out.println("let's install sim card");

        MiniSimCard miniSimCard = new MiniSimCard("123-456");
        Sim sim = new MiniSimCardAdapter(miniSimCard);

        Device d = new Device();
        d.install(sim);

    }
}
