package pl.bsb.gof.soccer;


import pl.bsb.gof.soccer.match.Match;
import pl.bsb.gof.soccer.team.Coach;
import pl.bsb.gof.soccer.team.Team;
import pl.bsb.gof.soccer.team.players.GermanPlayersPool;
import pl.bsb.gof.soccer.team.players.PolishPlayersPool;

public class MatchStarter {

    public static void main(String[] args) {

        Coach brzeczek = new Coach("Polska", PolishPlayersPool.getInstance());
        Coach loew = new Coach("Niemcy", GermanPlayersPool.getInstance());

        Team polska = brzeczek.constructTeam();
        Team niemcy = loew.constructTeam();

        Match match = new Match(polska, niemcy);

        match.introduction();
        match.play();
        match.showResult();
    }
}
