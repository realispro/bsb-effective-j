package pl.bsb.gof.soccer.match;


import pl.bsb.gof.soccer.team.Team;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class Match {

    private Team team1;

    private Team team2;

    private Map<Team, Integer> result = new HashMap<>();


    private EventHandler eventHandler;

    public Match(Team team1, Team team2) {
        this.team1 = team1;
        this.team2 = team2;
        result.put(team1, 0);
        result.put(team2, 0);

        eventHandler = new GoalHandler();
        eventHandler.setNext(new RedCardHandler());


    }

    public void introduction(){
        System.out.println("[ Presenting teams ]\n");
        System.out.println(team1.getName() + " vs " + team2.getName() + "\n");
        System.out.println(team1.info());
        System.out.println(team2.info());
    }


    public void score(Team t){
        Integer r = result.get(t);
        result.put(t, ++r);
    }


    public void play(){

        // TODO emulate game

        int eventCount = new Random(new Date().getTime()).nextInt(50);

        for(int i=0; i<eventCount; i++){
            Event event = EventGenerator.generateEvent(this);
            System.out.println("event: " + event);
            eventHandler.handleEvent(event);
        }


    }




    public void showResult() {

        int team1result = result.get(team1);
        int team2result = result.get(team2);

        System.out.println("\n" + team1.getName() + " : " + team2.getName());
        System.out.println(team1result + " : " + team2result);

        if(team1result==team2result){
            System.out.println("There was a draw");
        } else {
            Team won = team1result>team2result ? team1 : team2;
            Team lost = team1result>team2result ? team2 : team1;

            System.out.println("And the winner is: \n" + won);
            System.out.println("Sorry, boys: \n" + lost);
        }
    }

    public Team getTeam1() {
        return team1;
    }

    public Team getTeam2() {
        return team2;
    }





}
