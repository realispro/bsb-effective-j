package pl.bsb.gof.soccer.match;


import pl.bsb.gof.soccer.team.Player;
import pl.bsb.gof.soccer.team.Team;

public class Event {

    public EventType type;
    public Team team;
    public Player player;
    public Match match;

    public Event(EventType type, Match match, Team team, Player player) {
        this.type = type;
        this.match = match;
        this.team = team;
        this.player = player;
    }

    @Override
    public String toString() {
        return "Event{" +
                "type=" + type +
                ", team=" + team.getName() +
                ", player=" + player +
                '}';
    }
}
