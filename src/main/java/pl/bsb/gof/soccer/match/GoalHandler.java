package pl.bsb.gof.soccer.match;

import pl.bsb.gof.soccer.team.Team;

public class GoalHandler extends EventHandler{

    @Override
    public void handleEvent(Event e) {
        if(e.type==EventType.GOAL){
            Team t = e.team;
            e.match.score(t);
        } else {
            super.handleEvent(e);
        }
    }
}
