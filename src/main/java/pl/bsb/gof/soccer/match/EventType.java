package pl.bsb.gof.soccer.match;

public enum EventType {
    GOAL,
    YELLOW_CARD,
    RED_CARD
}