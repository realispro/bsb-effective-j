package pl.bsb.gof.soccer.match;

import pl.bsb.gof.soccer.team.Team;

public class RedCardHandler extends EventHandler{

    @Override
    public void handleEvent(Event e) {
        if(e.type==EventType.RED_CARD){
            Team t = e.team;
            t.getAttackers().remove(e.player);
            t.getMidfields().remove(e.player);
            t.getDefenders().remove(e.player);
            System.out.println("RED CARD! for player " + e.player);
        } else {
            super.handleEvent(e);
        }
    }
}
