package pl.bsb.gof.soccer.match;

public abstract class EventHandler {

    private EventHandler next;

    public void handleEvent(Event e){
        if(next!=null){
            next.handleEvent(e);
        }
    }

    public void setNext(EventHandler next) {
        this.next = next;
    }
}
