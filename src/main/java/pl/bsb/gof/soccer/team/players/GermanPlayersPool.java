package pl.bsb.gof.soccer.team.players;

import pl.bsb.gof.soccer.team.Player;

import java.util.Arrays;
import java.util.List;

public class GermanPlayersPool implements PlayersPool {

    private static GermanPlayersPool instance = new GermanPlayersPool();

    private GermanPlayersPool(){}

    public static GermanPlayersPool getInstance() {
        return instance;
    }


    public static List<Player> goalKeepers = Arrays.asList(
            new Player("Wojciech", "Neuer", 1, false)
    );

    public static List<Player> defenders = Arrays.asList(
            new Player("Mats", "Hummels", 120, false),
            new Player("Jonas", "Hector", 19, false),
            new Player("Jerome", "Boateng", 19, false),
            new Player("Emre", "Can", 19, false)
    );

    public static List<Player> midfields = Arrays.asList(
            new Player("Toni", "Kroos", 120, false),
            new Player("X", "Goetze", 122, false),
            new Player("X", "Szweinsteinger", 88, false),
            new Player("Mesut", "Ozil", 88, false),
            new Player("Marco", "Reus", 88, false),
            new Player("Sami", "Khedira", 19, false)
    );

    static List<Player> attackers = Arrays.asList(
            new Player("X", "Ballack", 2, false),
            new Player("Tomas", "Mueller", 12, false),
            new Player("Lukas", "Podolski", 19, false),
            new Player("Mario", "Gomez", 19, false)
    );


    @Override
    public List<Player> getGoalKeepers() {
        return goalKeepers;
    }

    @Override
    public List<Player> getDefenders() {
        return defenders;
    }

    @Override
    public List<Player> getMidfields() {
        return midfields;
    }

    @Override
    public List<Player> getAttackers() {
        return attackers;
    }
}
