package pl.bsb.gof.soccer.team;

import pl.bsb.gof.soccer.team.players.PlayersPicker;
import pl.bsb.gof.soccer.team.players.PlayersPool;

public class Coach {

    private String name;

    private PlayersPool pool;

    public Coach(String name, PlayersPool pool) {
        this.name = name;
        this.pool = pool;
    }

    public Team constructTeam(){

        PlayersPicker picker = new PlayersPicker(pool, false);
        TeamBuilder builder = new TeamBuilder();
        builder
                .addName(name)
                .addGoalKeeper(picker.getGoalKeeper())
                .addDefenders(picker.getDefenders(4))
                .addMidfields(picker.getMidfields(4))
                .addAttackers(picker.getAttackers(2));

        return builder.construct();
    }
}
