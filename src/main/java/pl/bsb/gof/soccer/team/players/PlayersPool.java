package pl.bsb.gof.soccer.team.players;

import pl.bsb.gof.soccer.team.Player;

import java.util.List;

public interface PlayersPool {

    List<Player> getGoalKeepers();

    List<Player> getDefenders();

    List<Player> getMidfields();

    List<Player> getAttackers();


}
