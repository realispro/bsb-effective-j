package pl.bsb.gof.soccer.team.players;


import pl.bsb.gof.soccer.team.Player;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;


public class PlayersPicker {

    private PlayersPool players;

    private boolean localOnly;

    public PlayersPicker(PlayersPool players, boolean localOnly) {
        this.players = players;
        this.localOnly = localOnly;
    }

    public Player getGoalKeeper() {
        return findByRank(
                players.getGoalKeepers(), 1, !localOnly)
                .stream().findFirst()
                .orElseThrow(() -> new IllegalStateException("no goalkeeper"));
    }

    public List<Player> getDefenders(int amount) {
        return findByRank(players.getDefenders(), amount, !localOnly);
    }

    public List<Player> getMidfields(int amount) {
        return findByRank(players.getMidfields(), amount, !localOnly);
    }

    public List<Player> getAttackers(int amount) {
        return findByRank(players.getAttackers(), amount, !localOnly);
    }

    private List<Player> findByRank(List<Player> players, int amount, boolean includePlayingAbroad) {
        Stream<Player> stream = players.stream();
        if (!includePlayingAbroad) {
            stream = stream.filter(p -> !p.isPlaysAbroad());
        }
        return stream.sorted((p1, p2) -> p1.getRank() - p2.getRank()).limit(amount).collect(Collectors.toList());
    }
}
