package pl.bsb.gof.creational.builder.pizza;

public class PizzaMan {

    private Pizza pizza;

    private boolean doughAdded;

    public PizzaMan(){
        pizza = new Pizza();
    }

    public PizzaMan addDough(String dough){
        pizza.setDough(dough);
        doughAdded = true;
        return this;
    }

    public PizzaMan addTopping(String topping){
        pizza.addTopping(topping);
        return this;
    }

    public PizzaMan addSause(String sause){
        pizza.setSause(sause);
        return this;
    }

    public Pizza bake(){
        if(!doughAdded){
            throw new IllegalStateException("define dough!");
        }
        return pizza;
    }





}
