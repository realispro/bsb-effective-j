package pl.bsb.gof.creational.builder.task;

public class TaskStarter {

    public static void main(String[] args) {
        System.out.println("TaskStarter.main");

        TaskBuilder builder = new TaskBuilder();
        Task task = builder.addTitle("user service bug")
                .addDescription("problem when creating new user")
                .addPriority(1)
                .createTask();
        System.out.println("task created: " + task);
    }

}
