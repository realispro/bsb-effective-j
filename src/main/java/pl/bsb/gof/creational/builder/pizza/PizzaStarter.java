package pl.bsb.gof.creational.builder.pizza;

public class PizzaStarter {

    public static void main(String[] args) {
        System.out.println("PizzaStarter.main");

        PizzaMan pizzaMan = new PizzaMan();
        pizzaMan.addDough("grube").addTopping("kurczak").addSause("keczup");
        Pizza pizza = pizzaMan.bake();

        System.out.println("eating pizza " + pizza);
    }
}
