package pl.bsb.gof.creational.builder.task;
/**
 * @author jjarzebowska
 */
public class TaskBuilder {

    private Task task;

    public TaskBuilder(){
        task = new Task();
    }

    public TaskBuilder addTitle(String title){
        task.setTitle(title);
        return this;
    }
    public TaskBuilder addDescription(String description){
        task.setDescription(description);
        return this;
    }
    public TaskBuilder addPriority(int priority){
        task.setPriority(priority);
        return this;
    }
    public TaskBuilder isClosed(boolean closed){
        task.setClosed(closed);
        return this;
    }
    public Task createTask(){
        return task;
    }
}