package pl.bsb.gof.creational.factory.abstractfactory.restaurant.mex;

import pl.bsb.gof.creational.factory.abstractfactory.restaurant.Appetizer;
import pl.bsb.gof.creational.factory.abstractfactory.restaurant.Dessert;
import pl.bsb.gof.creational.factory.abstractfactory.restaurant.MainDish;
import pl.bsb.gof.creational.factory.abstractfactory.restaurant.Restaurant;

public class MexRestaurant implements Restaurant {

    @Override
    public Appetizer getAppetizer() {
        return new Nachos();
    }
    @Override
    public MainDish getMainDish() {
        return new Burrito();
    }
    @Override
    public Dessert getDessert() {
        return new Churros();
    }
}
