package pl.bsb.gof.creational.factory.abstractfactory.restaurant;

public interface Restaurant {

    Appetizer getAppetizer();

    MainDish getMainDish();

    Dessert getDessert();
}
