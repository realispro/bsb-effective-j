package pl.bsb.gof.creational.factory.method;

public class Somersby implements Beer {
    @Override
    public float getVoltage() {
        return 4.5F;
    }
    @Override
    public String getName() {
        return "Somersby Apple";
    }
}
