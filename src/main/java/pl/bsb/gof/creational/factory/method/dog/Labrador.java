package pl.bsb.gof.creational.factory.method.dog;

public class Labrador extends Dog{

    public Labrador(String name) {
        super(name);
    }
}
