package pl.bsb.gof.creational.factory.abstractfactory.zoo;

public interface Bird {

    void fly();
}
