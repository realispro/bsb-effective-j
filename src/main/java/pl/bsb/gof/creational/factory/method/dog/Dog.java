package pl.bsb.gof.creational.factory.method.dog;

public abstract class Dog {

    private String name;

    public Dog(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
