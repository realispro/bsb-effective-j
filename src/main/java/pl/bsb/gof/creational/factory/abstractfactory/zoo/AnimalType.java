package pl.bsb.gof.creational.factory.abstractfactory.zoo;

public enum AnimalType {

    VEGE,
    PREDATOR
}
