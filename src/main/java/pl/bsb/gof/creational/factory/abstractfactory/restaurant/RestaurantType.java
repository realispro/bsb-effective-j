package pl.bsb.gof.creational.factory.abstractfactory.restaurant;

public enum RestaurantType {

    MCDONALDS,
    MEXICAN,
    ITALIAN
}
