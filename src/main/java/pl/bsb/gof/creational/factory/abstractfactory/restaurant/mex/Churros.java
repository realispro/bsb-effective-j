package pl.bsb.gof.creational.factory.abstractfactory.restaurant.mex;

import pl.bsb.gof.creational.factory.abstractfactory.restaurant.Dessert;

public class Churros implements Dessert {
    @Override
    public void enjoy() {
        System.out.println("Enjoy mex dessert");
    }
}
