package pl.bsb.gof.creational.factory.abstractfactory.zoo.predators;

import pl.bsb.gof.creational.factory.abstractfactory.zoo.Fish;

public class Shark implements Fish {
    @Override
    public void swim() {
        System.out.println("shark is swimming very fast");
    }
}
