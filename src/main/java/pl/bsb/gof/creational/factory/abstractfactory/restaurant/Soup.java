package pl.bsb.gof.creational.factory.abstractfactory.restaurant;

public interface Soup {

    void drink();
}
