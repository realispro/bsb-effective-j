package pl.bsb.gof.creational.factory.method;

public class Ipa implements Beer {
    @Override
    public float getVoltage() {
        return 3.5F;
    }
    @Override
    public String getName() {
        return "Żywiec IPA";
    }
}
