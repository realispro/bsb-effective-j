package pl.bsb.gof.creational.factory.abstractfactory.restaurant;

import pl.bsb.gof.creational.factory.abstractfactory.restaurant.italian.ItalianRestaurant;
import pl.bsb.gof.creational.factory.abstractfactory.restaurant.mex.MexRestaurant;

public class RestaurantFactory {
    public static Restaurant provideRestaurant(RestaurantType restaurantType){
        switch (restaurantType){
            case ITALIAN:
                return new ItalianRestaurant();
            case MEXICAN:
                return new MexRestaurant();
        }
        return new ItalianRestaurant();
    }
}
