package pl.bsb.gof.creational.factory.abstractfactory.restaurant;

public interface Dessert {

    void enjoy();
}
