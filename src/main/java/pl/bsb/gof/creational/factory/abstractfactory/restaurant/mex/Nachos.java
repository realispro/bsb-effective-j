package pl.bsb.gof.creational.factory.abstractfactory.restaurant.mex;

import pl.bsb.gof.creational.factory.abstractfactory.restaurant.Appetizer;

public class Nachos implements Appetizer {
    @Override
    public void taste() {
        System.out.println("Taste mex appetizer");
    }
}
