package pl.bsb.gof.creational.factory.abstractfactory.restaurant;

public interface Appetizer {

    void taste();
}
