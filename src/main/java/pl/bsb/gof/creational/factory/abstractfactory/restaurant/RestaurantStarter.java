package pl.bsb.gof.creational.factory.abstractfactory.restaurant;

public class RestaurantStarter {

    public static void main(String[] args) {
        System.out.println("RestaurantStarter.main");

        RestaurantType type = RestaurantType.MEXICAN;

        Restaurant restaurant = RestaurantFactory.provideRestaurant(type);
        Appetizer appetizer = restaurant.getAppetizer();
        appetizer.taste();

        MainDish mainDish = restaurant.getMainDish();
        mainDish.eat();

        Dessert dessert = restaurant.getDessert();
        dessert.enjoy();

    }
}
