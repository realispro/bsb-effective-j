package pl.bsb.gof.creational.factory.abstractfactory.zoo;

public interface Mammal {

    void move();
}
