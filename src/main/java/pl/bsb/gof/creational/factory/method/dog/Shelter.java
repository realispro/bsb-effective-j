package pl.bsb.gof.creational.factory.method.dog;

// dog creator - factory
public class Shelter {


    public Dog provideDog(String size){

        switch (size){
            case "small":
                return new Poodle("Jackie");
            case "big":
                return new Labrador("Reksio");
        }

        return null;
    }


}
