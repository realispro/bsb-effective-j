package pl.bsb.gof.creational.factory.abstractfactory.zoo.predators;

import pl.bsb.gof.creational.factory.abstractfactory.zoo.AnimalProvider;
import pl.bsb.gof.creational.factory.abstractfactory.zoo.Bird;
import pl.bsb.gof.creational.factory.abstractfactory.zoo.Fish;
import pl.bsb.gof.creational.factory.abstractfactory.zoo.Mammal;

public class PredatorsProvider implements AnimalProvider {
    @Override
    public Bird provideBird() {
        return new Eagle();
    }

    @Override
    public Fish provideFish() {
        return new Shark();
    }

    @Override
    public Mammal provideMammal() {
        return new Wolf();
    }
}
