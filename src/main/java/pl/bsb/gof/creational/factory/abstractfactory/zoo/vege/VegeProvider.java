package pl.bsb.gof.creational.factory.abstractfactory.zoo.vege;

import pl.bsb.gof.creational.factory.abstractfactory.zoo.AnimalProvider;
import pl.bsb.gof.creational.factory.abstractfactory.zoo.Bird;
import pl.bsb.gof.creational.factory.abstractfactory.zoo.Fish;
import pl.bsb.gof.creational.factory.abstractfactory.zoo.Mammal;

public class VegeProvider implements AnimalProvider {
    @Override
    public Bird provideBird() {
        return new Pigeon();
    }

    @Override
    public Fish provideFish() {
        return new Tuna();
    }

    @Override
    public Mammal provideMammal() {
        return new Gorilla();
    }
}
