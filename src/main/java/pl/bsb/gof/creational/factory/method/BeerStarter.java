package pl.bsb.gof.creational.factory.method;

public class BeerStarter {

    public static void main(String[] args) {
        System.out.println("BeerStarter.main");

        BeerType beerType = BeerType.FRUITY;
        Beer beer = Pub.orderBeer(beerType);

        System.out.println("drinking beer " + beer.getName());
    }
}
