package pl.bsb.gof.creational.factory.method;

public class Pub {
    public static Beer orderBeer(BeerType beerType) {
        switch (beerType) {
            case PILS:
                return new Pilsner();
            case IPA:
                return new Ipa();
            case FRUITY:
                return new Somersby();
        }
        return new Pilsner();
    }
}
