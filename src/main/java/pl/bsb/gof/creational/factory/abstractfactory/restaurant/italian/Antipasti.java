package pl.bsb.gof.creational.factory.abstractfactory.restaurant.italian;

import pl.bsb.gof.creational.factory.abstractfactory.restaurant.Appetizer;

public class Antipasti implements Appetizer {
    @Override
    public void taste() {
        System.out.println("Taste italian appetizer");
    }
}
