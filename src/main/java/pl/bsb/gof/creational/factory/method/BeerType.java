package pl.bsb.gof.creational.factory.method;

public enum BeerType {
    PILS,
    IPA,
    FRUITY
}
