package pl.bsb.gof.creational.factory.abstractfactory.restaurant.italian;

import pl.bsb.gof.creational.factory.abstractfactory.restaurant.Appetizer;
import pl.bsb.gof.creational.factory.abstractfactory.restaurant.Dessert;
import pl.bsb.gof.creational.factory.abstractfactory.restaurant.MainDish;
import pl.bsb.gof.creational.factory.abstractfactory.restaurant.Restaurant;

public class ItalianRestaurant implements Restaurant {
    @Override
    public Appetizer getAppetizer() {
        return new Antipasti();
    }
    @Override
    public MainDish getMainDish() {
        return new Pasta();
    }
    @Override
    public Dessert getDessert() {
        return new Cannoli();
    }
}
