package pl.bsb.gof.creational.factory.abstractfactory.zoo.vege;

import pl.bsb.gof.creational.factory.abstractfactory.zoo.Mammal;

public class Gorilla implements Mammal {
    @Override
    public void move() {
        System.out.println("gorilla is jumping");
    }
}
