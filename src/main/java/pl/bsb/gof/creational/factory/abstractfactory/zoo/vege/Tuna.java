package pl.bsb.gof.creational.factory.abstractfactory.zoo.vege;

import pl.bsb.gof.creational.factory.abstractfactory.zoo.Fish;

public class Tuna implements Fish {
    @Override
    public void swim() {
        System.out.println("tuna is swimming");
    }
}
