package pl.bsb.gof.creational.factory.abstractfactory.zoo.vege;

import pl.bsb.gof.creational.factory.abstractfactory.zoo.Bird;

public class Pigeon implements Bird {
    @Override
    public void fly() {
        System.out.println("pigeon is flying");
    }
}
