package pl.bsb.gof.creational.factory.abstractfactory.zoo;

public interface AnimalProvider {

    Bird provideBird();

    Fish provideFish();

    Mammal provideMammal();

}
