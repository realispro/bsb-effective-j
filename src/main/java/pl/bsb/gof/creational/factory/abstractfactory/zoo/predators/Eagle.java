package pl.bsb.gof.creational.factory.abstractfactory.zoo.predators;

import pl.bsb.gof.creational.factory.abstractfactory.zoo.Bird;

public class Eagle implements Bird {
    @Override
    public void fly() {
        System.out.println("eagle is flying high");
    }
}
