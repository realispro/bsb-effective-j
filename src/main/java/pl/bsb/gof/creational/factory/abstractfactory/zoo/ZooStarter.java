package pl.bsb.gof.creational.factory.abstractfactory.zoo;

import pl.bsb.gof.creational.factory.abstractfactory.zoo.predators.PredatorsProvider;
import pl.bsb.gof.creational.factory.abstractfactory.zoo.vege.VegeProvider;

public class ZooStarter {

    public static void main(String[] args) {
        System.out.println("ZooStarter.main");

        AnimalType type = AnimalType.PREDATOR;

        AnimalProvider provider = getAnimalProvider(type);
        Bird b = provider.provideBird();
        Fish f = provider.provideFish();
        Mammal m = provider.provideMammal();

        b.fly();
        f.swim();
        m.move();
    }

    public static AnimalProvider getAnimalProvider(AnimalType type){
        switch (type){
            case PREDATOR:
                return new PredatorsProvider();
            default:
                return new VegeProvider();
        }
    }

}
