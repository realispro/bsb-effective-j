package pl.bsb.gof.creational.factory.abstractfactory.restaurant;

public interface MainDish {

    void eat();
}
