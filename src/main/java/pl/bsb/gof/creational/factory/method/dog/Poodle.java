package pl.bsb.gof.creational.factory.method.dog;

public class Poodle extends Dog {

    public Poodle(String name) {
        super(name);
    }
}
