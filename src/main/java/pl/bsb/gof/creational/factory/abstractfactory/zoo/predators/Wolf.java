package pl.bsb.gof.creational.factory.abstractfactory.zoo.predators;

import pl.bsb.gof.creational.factory.abstractfactory.zoo.Mammal;

public class Wolf implements Mammal {
    @Override
    public void move() {
        System.out.println("wolf is running very quiet");
    }
}
