package pl.bsb.gof.creational.factory.abstractfactory.restaurant.mex;

import pl.bsb.gof.creational.factory.abstractfactory.restaurant.MainDish;

public class Burrito implements MainDish {
    @Override
    public void eat() {
        System.out.println("eat mex main dish");
    }
}
