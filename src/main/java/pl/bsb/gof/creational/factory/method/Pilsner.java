package pl.bsb.gof.creational.factory.method;

public class Pilsner implements Beer {
    @Override
    public float getVoltage() {
        return 5.0F;
    }
    @Override
    public String getName() {
        return "Pilsner Urquell";
    }
}
