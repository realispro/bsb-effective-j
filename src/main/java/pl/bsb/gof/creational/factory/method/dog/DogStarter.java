package pl.bsb.gof.creational.factory.method.dog;

public class DogStarter {

    public static void main(String[] args) {
        System.out.println("DogStarter.main");

        Shelter shelter = new Shelter();

        Dog dog = shelter.provideDog("big");
        System.out.println("dog name: " + dog.getName());

    }
}
