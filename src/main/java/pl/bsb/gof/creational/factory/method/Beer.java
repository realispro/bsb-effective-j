package pl.bsb.gof.creational.factory.method;

public interface Beer {

    float getVoltage();

    String getName();

}
