package pl.bsb.gof.creational.factory.abstractfactory.zoo;

public interface Fish {

    void swim();

}
