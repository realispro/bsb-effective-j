package pl.bsb.gof.creational.factory.abstractfactory.restaurant.italian;

import pl.bsb.gof.creational.factory.abstractfactory.restaurant.MainDish;

public class Pasta implements MainDish {
    @Override
    public void eat() {
        System.out.println("eat italian main dish");
    }
}
