package pl.bsb.gof.creational.factory.abstractfactory.restaurant.italian;

import pl.bsb.gof.creational.factory.abstractfactory.restaurant.Dessert;

public class Cannoli implements Dessert {
    @Override
    public void enjoy() {
        System.out.println("enjoy italian dessert");
    }
}
