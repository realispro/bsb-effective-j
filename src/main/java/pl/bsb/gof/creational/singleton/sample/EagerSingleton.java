package pl.bsb.gof.creational.singleton.sample;

public class EagerSingleton {

    private static EagerSingleton instance;

    static {
        try {
            instance = new EagerSingleton();
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    private EagerSingleton() throws Exception{}

    public static EagerSingleton getInstance(){
        return instance;
    }


}
