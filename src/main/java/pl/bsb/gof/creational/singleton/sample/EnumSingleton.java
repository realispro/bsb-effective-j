package pl.bsb.gof.creational.singleton.sample;

public enum EnumSingleton {

    INSTANCE;

    boolean admin;

    public static EnumSingleton getInstance() {
        return INSTANCE;
    }

    public boolean isAdmin() {
        return admin;
    }

    public void setAdmin(boolean admin) {
        this.admin = admin;
    }
}
