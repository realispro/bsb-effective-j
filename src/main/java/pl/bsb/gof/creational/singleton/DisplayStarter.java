package pl.bsb.gof.creational.singleton;

public class DisplayStarter {

    public static void main(String[] args) {
        System.out.println("DisplayStarter.main");

        Display display = Display.getInstance();
        Display display1 = Display.getInstance();

        display.show("important message");
        display1.show("more important message");
    }
}
