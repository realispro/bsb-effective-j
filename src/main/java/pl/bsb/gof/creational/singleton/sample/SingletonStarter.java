package pl.bsb.gof.creational.singleton.sample;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

public class SingletonStarter {

    public static void main(String[] args) {
        System.out.println("SingletonStarter.main");

        EagerSingleton instance = EagerSingleton.getInstance();
        EagerSingleton instance2 = EagerSingleton.getInstance();
        System.out.println("instance: " + instance + ", instance2: " + instance2);

        LazySingleton lazyInstance = LazySingleton.getInstance();
        LazySingleton lazyInstance2 = null;

        try {
            Constructor<LazySingleton> constructor =  LazySingleton.class.getDeclaredConstructor();
            constructor.setAccessible(true);
            lazyInstance2 = constructor.newInstance();

        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
        System.out.println("lazy: " + lazyInstance + ", lazy2: " + lazyInstance2);

        EnumSingleton enumSingleton = EnumSingleton.getInstance();
        EnumSingleton enumSingleton2 = EnumSingleton.getInstance();
        System.out.println("enum: " + enumSingleton + ", enum2: " + enumSingleton2);

    }

}
