package pl.bsb.gof.creational.singleton;


public class Display {

    private static Display displayInstance;

    private Display() {
    }

    //  Eager
//    private static final Display instance = new Display();
//    public static Display getInstance() {
//        return instance;
//    }
//  Lazy

    public static Display getInstance() {
        if (null == displayInstance) {
            displayInstance = new Display();
        }
        return displayInstance;
    }

    public void show(String text) {
        System.out.println("[" + text + "]");
    }

}