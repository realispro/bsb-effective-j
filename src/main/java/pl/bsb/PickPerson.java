package pl.bsb;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Random;

public class PickPerson {

    public static void main(String[] args) {

        List<Integer> numbers = Arrays.asList(1,2,3,4,5,6,7,8,9,10,11,12);
        System.out.println("Choosing random person of " + numbers);

        int random = numbers.get(
                new Random(new Date().getTime()).nextInt(numbers.size())
        );
        System.out.println("Person number " + random);

    }
}
