package pl.bsb.solid;

public abstract class Management extends Employee {

    public Management(int id, String firstName, String lastName, int grade) {
        super(id, firstName, lastName, grade);
    }

    public void sendAnouncement(String anouncement) {
        System.out.println("sending anouncement: " + anouncement);
    }
}
