package pl.bsb.solid;



public class TechnicalStaff extends Employee implements SystemOptimizer, IncidentHandler {

    private String title;

    public TechnicalStaff(int id, String firstName, String lastName, String title, int grade) {
        super(id, firstName, lastName, grade);
        this.title = title;
    }

    public void maintainSystem(String system){
        System.out.println("[" + getId() +"] maintaining system " + system );
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        return "TechnicalStaff{" +
                "title='" + title + '\'' +
                '}' +
                super.toString();
    }

    @Override
    public void optimizeSystem(String system) {
        System.out.println("optimzing system " + system);
    }

    @Override
    public void handleIncident(String incident, IncidentHandler support) {
        System.out.println("handling incident " + incident);
    }
}
