package pl.bsb.solid;

public class SolidStarter {

    public static void main(String[] args) {
        System.out.println("SolidStarter.main");

        Director director = new Director(102, "Adam", "Nowak");

        Manager manager = new Manager(101, "Jan", "Kowalski", 8);

        TechnicalStaffBuilder builder = new TechnicalStaffBuilder();


        builder = builder.addId(1).addFirstName("Jack").addLastName("Black")
                .addTitle("db admin").addGrade(3);
        TechnicalStaff staff = builder.build();
        //new TechnicalStaff(1, "Jack", "Black", "db admin", 3);

        staff.maintainSystem("critical system");

        Accountant accountant = new Accountant(2, "Joe", "Doe", 4);
        accountant.calculate();

        IncidentHandler support = staff;


        String incident = "very critical incident";
        IncidentHandler incidentHandler = new Intern();
        incidentHandler.handleIncident(incident, support);

        String system = "non optimized system";
        SystemOptimizer optimizer = staff;
        optimizer.optimizeSystem(system);

        Management management = manager;

        String message = "rating time!";
        management.sendAnouncement(message);

        manager.performRating(staff);
        manager.promote(staff);

        manager.performRating(accountant);
        manager.promote(accountant);

        System.out.println("staff: " + staff);

    }

}
