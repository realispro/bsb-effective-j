package pl.bsb.solid;

public class Accountant extends Employee {

    public Accountant(int id, String firstName, String lastName, int grade) {
        super(id, firstName, lastName, grade);
    }

    public void calculate(){
        System.out.println("calculating...");
    }

    @Override
    public String toString() {
        return "Accountant: " + super.toString();
    }
}
