package pl.bsb.solid;

public class TechnicalStaffBuilder {

    private int id;

    private String firstName;

    private String lastName;

    private String title;

    private int grade;

    public TechnicalStaffBuilder() {
    }

    public TechnicalStaffBuilder addId(int id){
        this.id = id;
        return this;
    }

    public TechnicalStaffBuilder addFirstName(String firstName){
        this.firstName = firstName;
        return this;
    }

    public TechnicalStaffBuilder addLastName(String lastName){
        this.lastName = lastName;
        return this;
    }

    public TechnicalStaffBuilder addTitle(String title){
        this.title = title;
        return this;
    }

    public TechnicalStaffBuilder addGrade(int grade){
        this.grade = grade;
        return this;
    }

    public TechnicalStaff build(){
        return new TechnicalStaff(id, firstName, lastName, title, grade);
    }





}
