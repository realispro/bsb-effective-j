package pl.bsb.solid;

@FunctionalInterface
public interface IncidentHandler {

    void handleIncident(String incident, IncidentHandler support);
}
