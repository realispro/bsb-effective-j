package pl.bsb.solid;

import java.util.Date;
import java.util.Random;

public class Manager extends Management{

    public Manager(int id, String firstName, String lastName, int grade) {
        super(id, firstName, lastName, grade);
    }

    public void performRating(Employee employee){
        employee.setRate( new Random(new Date().getTime()).nextInt(10) + 1 );
    }

    public void promote(Employee employee){
        if(employee.getRate()>=8){
            employee.setGrade(employee.getGrade()+1);
        }
    }

}
