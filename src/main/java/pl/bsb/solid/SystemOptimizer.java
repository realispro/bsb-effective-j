package pl.bsb.solid;

public interface SystemOptimizer {

    void optimizeSystem(String system);

}
